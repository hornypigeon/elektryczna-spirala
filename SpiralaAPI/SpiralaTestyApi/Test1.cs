﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;


namespace SpiralaTestyApi
{
	class Test1
	{
		private IWebDriver driver;

		private string mail_a = "selenium1@wp.pl";
		private string haslo_a = "selenium1";

		private string mail = "selenium6@wp.pl";
		private string haslo = "selenium1";

		private void startBrowser()
		{
			driver = new ChromeDriver("./");

			driver.Url = "http://localhost:4200";
			System.Threading.Thread.Sleep(9000);
		}

		private void stopBrowser()
		{
			driver.Close();
		}

		public void test1()
		{
			startBrowser();

			driver.FindElement(By.XPath("//a[contains(text(), 'Zaloguj')]")).Click();
			System.Threading.Thread.Sleep(3000);
			driver.FindElement(By.XPath("//a[contains(text(), 'Zarejestruj się!')]")).Click();
			System.Threading.Thread.Sleep(3000);
			driver.FindElement(By.CssSelector("input[id=emailId]")).SendKeys(mail);
			driver.FindElement(By.CssSelector("input[id=password]")).SendKeys(haslo);
			System.Threading.Thread.Sleep(2000);
			driver.FindElement(By.CssSelector("button[id=signUpButton]")).Click();

			System.Threading.Thread.Sleep(10000);
			driver.FindElement(By.XPath("//a[contains(text(), 'Wyloguj')]")).Click();
			System.Threading.Thread.Sleep(1500);
			driver.FindElement(By.XPath("//a[contains(text(), 'Zaloguj')]")).Click();
			System.Threading.Thread.Sleep(2000);
			driver.FindElement(By.CssSelector("input[id=loginEmailId]")).SendKeys(mail);
			driver.FindElement(By.CssSelector("input[id=loginPassword]")).SendKeys(haslo);
			driver.FindElement(By.CssSelector("button[id=loginButton]")).Click();
			System.Threading.Thread.Sleep(10000);
			driver.FindElement(By.XPath("//a[contains(text(), 'Moje konto')]")).Click();
			System.Threading.Thread.Sleep(3500);

			stopBrowser();
		}

		public void test2()
		{
			startBrowser();
			string nazwaKat = "testy-selenium";

			driver.FindElement(By.XPath("//a[contains(text(), 'Zaloguj')]")).Click();
			System.Threading.Thread.Sleep(3000);
			driver.FindElement(By.CssSelector("input[id=loginEmailId]")).SendKeys(mail_a);
			driver.FindElement(By.CssSelector("input[id=loginPassword]")).SendKeys(haslo_a);
			driver.FindElement(By.CssSelector("button[id=loginButton]")).Click();
			System.Threading.Thread.Sleep(10000);
			driver.FindElement(By.XPath("//a[contains(text(), 'Nasze produkty')]")).Click();
			System.Threading.Thread.Sleep(5000);

			driver.FindElement(By.XPath("//button[contains(text(), 'Zarządzaj kategoriami')]")).Click();
			System.Threading.Thread.Sleep(2500);
			driver.FindElement(By.CssSelector("input[id=kategoriaName]")).SendKeys(nazwaKat);
			driver.FindElement(By.XPath("//button[contains(text(), 'Dodaj kategorię')]")).Click();
			System.Threading.Thread.Sleep(4500);


			driver.FindElement(By.XPath("//button[contains(text(), 'Dodaj Produkt')]")).Click();
			System.Threading.Thread.Sleep(2500);

			var kategoria = driver.FindElement(By.CssSelector("select[id=productSeller]"));
			var wybieranie_kategorii = new SelectElement(kategoria);

			driver.FindElement(By.CssSelector("input[id=productName]")).SendKeys("Selenium  " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")+ "AM");
			driver.FindElement(By.CssSelector("input[id=productCategory]")).SendKeys("Testowy");
			driver.FindElement(By.CssSelector("textarea[id=productDescription]")).SendKeys("Takie tam tesciki ooo!  \n" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")+ "AM");
			driver.FindElement(By.CssSelector("input[id=productPrice]")).SendKeys("3.99");
			driver.FindElement(By.CssSelector("input[id=productQuatity]")).SendKeys("7");
			wybieranie_kategorii.SelectByText(nazwaKat);
			driver.FindElement(By.CssSelector("input[id=productImageUrl]")).SendKeys("https://www.seleniumhq.org/images/big-logo.png");
			driver.FindElement(By.CssSelector("button[id=dodawanko]")).Click();
			System.Threading.Thread.Sleep(6500);

			var kategoriaOgolnie = driver.FindElement(By.CssSelector("select[id=brand_select]"));
			var wybieranie_kategoriiOgolnie = new SelectElement(kategoriaOgolnie);
			wybieranie_kategoriiOgolnie.SelectByText(nazwaKat);


			System.Threading.Thread.Sleep(5000);
			stopBrowser();
		}

		public void test3()
		{
			startBrowser();
			driver.FindElement(By.XPath("//a[contains(text(), 'Zaloguj')]")).Click();
			System.Threading.Thread.Sleep(3000);
			driver.FindElement(By.CssSelector("input[id=loginEmailId]")).SendKeys(mail);
			driver.FindElement(By.CssSelector("input[id=loginPassword]")).SendKeys(haslo);
			driver.FindElement(By.CssSelector("button[id=loginButton]")).Click();
			System.Threading.Thread.Sleep(10000);
			driver.FindElement(By.XPath("//a[contains(text(), 'Nasze produkty')]")).Click();
			System.Threading.Thread.Sleep(5000);
			driver.FindElement(By.CssSelector("a[class=ng-star-inserted]")).Click();
			driver.FindElement(By.XPath("/html/body/app-root/div/main/app-product-list/div[2]/div/div[2]/div/div[1]/div/div/div/div/div/div/span[2]/a")).Click();
			System.Threading.Thread.Sleep(1500);
			driver.FindElement(By.XPath("//*[@id='basicExampleNav']/ul[2]/li[2]/div/a")).Click();
			System.Threading.Thread.Sleep(1500);
			driver.FindElement(By.XPath("//a[contains(text(), 'Do kasy')]")).Click();
			System.Threading.Thread.Sleep(1500);
			driver.FindElement(By.XPath("//a[contains(text(), 'Dalej')]")).Click();
			System.Threading.Thread.Sleep(1500);

			driver.FindElement(By.CssSelector("input[id=firstName]")).SendKeys("ImieSelenium");
			driver.FindElement(By.CssSelector("input[id=lastName]")).SendKeys("NazwiskoSelenium");
			driver.FindElement(By.CssSelector("input[id=address]")).SendKeys("ul. Selenium 55");
			driver.FindElement(By.CssSelector("input[id=address2]")).SendKeys("VisualStudio");
			driver.FindElement(By.CssSelector("input[id=zip]")).SendKeys("22-333");

			var kraj = driver.FindElement(By.CssSelector("select[id=country]"));
			var woje = driver.FindElement(By.CssSelector("select[id=state]"));
			
			var wyb_kraj= new SelectElement(kraj);
			var wyb_woje = new SelectElement(woje);

			wyb_kraj.SelectByText("Polska");
			wyb_woje.SelectByText("Małopolska");

			driver.FindElement(By.XPath("//button[contains(text(), 'Przejdź dalej')]")).Click();
			System.Threading.Thread.Sleep(1500);

			driver.FindElement(By.CssSelector("input[id=firstName]")).SendKeys("ImieSelenium");
			driver.FindElement(By.CssSelector("input[id=lastName]")).SendKeys("NazwiskoSelenium");
			driver.FindElement(By.CssSelector("input[id=address]")).SendKeys("ul. Selenium 55");
			driver.FindElement(By.CssSelector("input[id=address2]")).SendKeys("VisualStudio");
			driver.FindElement(By.CssSelector("input[id=zip]")).SendKeys("22-333");

			var kraj2 = driver.FindElement(By.CssSelector("select[id=country]"));
			var woje2 = driver.FindElement(By.CssSelector("select[id=state]"));

			var wyb_kraj2 = new SelectElement(kraj2);
			var wyb_woje2 = new SelectElement(woje2);

			wyb_kraj2.SelectByText("Polska");
			wyb_woje2.SelectByText("Małopolska");
			driver.FindElement(By.XPath("//button[contains(text(), 'Przejdź dalej')]")).Click();
			System.Threading.Thread.Sleep(1500);
			driver.FindElement(By.XPath("//*[@id='receipt']/a")).Click();
			System.Threading.Thread.Sleep(4500);

			driver.FindElement(By.XPath("//*[@id='basicExampleNav']/ul[1]/li[1]/a")).Click();

			System.Threading.Thread.Sleep(5000);
			stopBrowser();
		}
	}
}
