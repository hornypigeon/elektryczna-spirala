import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ProductService } from 'src/app/shared/services/product.service';
import { Product } from 'src/app/shared/models/product';
import { ProductListComponent } from 'src/app/layouts/product/product-list/product-list.component'

declare var $: any;
declare var require: any;
declare var toastr: any;
const shortId = require('shortid');
const moment = require('moment');

@Component({
	selector: 'app-add-product',
	templateUrl: './add-product.component.html',
	styleUrls: [ './add-product.component.scss' ]
})
export class AddProductComponent implements OnInit {

	productList: Product[];
	loading = false;
	brands = ['All', 'Google', 'Apple', 'Realme', 'Nokia', 'Motorolla', 'Telewizory', 'Telefony'];
	kategoriaNameNew = '';
	selectedBrand: 'All';


	product: Product = new Product();
	constructor(private productService: ProductService,
		private listaKategorii: ProductListComponent) {}

	ngOnInit() {
		this.brands = this.listaKategorii.brands;
	}

	createProduct(productForm: NgForm) {
		productForm.value['productId'] = 'PROD_' + shortId.generate();
		productForm.value['productAdded'] = moment().unix();
		productForm.value['ratings'] = Math.floor(Math.random() * 5 + 1);
		if (productForm.value['productImageUrl'] === undefined) {
			productForm.value['productImageUrl'] = 'http://via.placeholder.com/640x360/007bff/ffffff';
		}

		productForm.value['favourite'] = false;

		const date = productForm.value['productAdded'];

		this.productService.createProduct(productForm.value);

		this.product = new Product();

		$('#exampleModalLong').modal('hide');

		toastr.success('product ' + productForm.value['productName'] + 'is added successfully', 'Product Creation');
	}

	addCategory(productForm: NgForm)
	{
		const index = this.listaKategorii.brands.indexOf(productForm.value['kategoriaName'], 0);
		if(index == -1)
		{
			this.listaKategorii.brands.push(productForm.value['kategoriaName']);
			console.log("Dodano kategorie!");
			toastr.success('Kategoria ' + productForm.value['kategoriaName'] + ' dodana prawidłowo!', 'Tworzenie kategorii');
			productForm.resetForm();
			$('#exampleModalCategory').modal('hide');
		}
		else 
		{
			toastr.error('Kategoria ' + productForm.value['kategoriaName'] + ' już istnieje!', 'Tworzenie kategorii');
			productForm.resetForm();
		}
	}

	delCategory()
	{
		const index = this.listaKategorii.brands.indexOf(this.selectedBrand, 0);
		if (index == 0)
		{
			toastr.error('Kategoria ' + this.selectedBrand + ' nie może być usunięta!', 'Usuwanie kategorii');
			this.selectedBrand = null;
		}
		else if(index > -1) 
		{
			this.listaKategorii.brands.splice(index, 1);
			toastr.warning('Kategoria ' + this.selectedBrand + ' usunięta prawidłowo!', 'Usuwanie kategorii');
			this.selectedBrand = null;
			$('#exampleModalCategory').modal('hide');
		}
	}

	editCategory()
	{
		const index = this.listaKategorii.brands.indexOf(this.selectedBrand, 0);
		if (index == 0)
		{
			toastr.error('Kategoria ' + this.selectedBrand + ' nie może być edytowana!', 'Edycja kategorii');
			this.selectedBrand = null;
			this.kategoriaNameNew = null;
		}
		else if(index > -1)
		{
			this.listaKategorii.brands[index]=this.kategoriaNameNew;
			toastr.success('Kategoria ' + this.selectedBrand + ' została zmieniona na: ' 
			+ this.kategoriaNameNew, 'Edycja kategorii');
			this.selectedBrand = null;
			this.kategoriaNameNew = null;
			$('#exampleModalCategory').modal('hide');
		} 
	}
}
