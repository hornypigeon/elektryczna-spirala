import { Component, OnInit } from '@angular/core';
import { Product } from '../../../shared/models/product';
import { User } from '../../../shared/models/user';
import { AuthService } from '../../../shared/services/auth.service';
import { ProductService } from '../../../shared/services/product.service';
import { ToastrService } from 'src/app/shared/services/toastr.service';
import { Shipping } from 'src/app/shared/models/shippings';
@Component({
	selector: 'app-product-list',
	templateUrl: './product-list.component.html',
	styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
	productList: Product[];
	shippingList: Shipping[];

	loading = false;
	brands = ['All', 'Telefony', 'Telewizory', 'Lodowki','Pralki', 'Konsole', 'Radia'];
	filteredItems = ['Samsung', 'Zelmer', 'Apple', 'iPhone', 'Nokia', 'Xbox'];

	
	selectedBrand: 'All';
	search: 'All';

	page = 1;
	constructor(
		public authService: AuthService,
		private productService: ProductService,
		private toastrService: ToastrService
	) { }

	ngOnInit() {
		this.getAllProducts();
		this.getAllShippings();
	}

	szukanie()
	{
		console.log("Funkcja odpalona!");
		console.log("Wartosc wpisana to: ", this.search);
	}

	getAllProducts() {
		// this.spinnerService.show();
		this.loading = true;
		const x = this.productService.getProducts();
		x.snapshotChanges().subscribe(
			(product) => {
				this.loading = false;
				// this.spinnerService.hide();
				this.productList = [];
				product.forEach((element) => {
					const y = element.payload.toJSON();
					y['$key'] = element.key;
					this.productList.push(y as Product);
				});
			},
			(err) => {
				this.toastrService.error('Error while fetching Products', err);
			}
		);
	}

	getAllShippings() {
		// this.spinnerService.show();
		this.loading = true;
		const x = this.productService.getShipping();
		x.snapshotChanges().subscribe(
			(product) => {
				this.loading = false;
				// this.spinnerService.hide();
				this.shippingList = [];
				product.forEach((element) => {
					const y = element.payload.toJSON();
					y['$key'] = element.key;
					this.shippingList.push(y as Shipping);
				});
				var address1 = this.shippingList[0];
				//var cipek = Object.values(address1.products)[0].productName;
				
			},
			(err) => {
				this.toastrService.error('Error while fetching Products', err);
			}
		);
	}


	removeProduct(key: string) {
		this.productService.deleteProduct(key);
	}

	addFavourite(product: Product) {
		this.productService.addFavouriteProduct(product);
	}

	addToCart(product: Product) {
		this.productService.addToCart(product);
	}
}
