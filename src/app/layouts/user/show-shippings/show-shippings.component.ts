import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ToastrService } from 'src/app/shared/services/toastr.service';
import { ProductService } from 'src/app/shared/services/product.service';
import { Shipping } from 'src/app/shared/models/shippings';
import { Product } from '../../../shared/models/product';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-show-shippings',
  templateUrl: './show-shippings.component.html',
  styleUrls: ['./show-shippings.component.scss']
})
export class ShowShippingsComponent implements OnInit {
  shippingList: Shipping[];
	shippingCurrentList:  Shipping[];
	
	currentProducts = [];
	currentZamowienie = [];
	numberOfShippings = 0;
	
  loading = false;

  constructor(
		public authService: AuthService,
		private productService: ProductService,
		private toastrService: ToastrService
	) { }

  ngOnInit() {

    this.getAllShippings();

  }

	getAllShippings() {
		// this.spinnerService.show();
		this.loading = true;
		const x = this.productService.getShipping();
		x.snapshotChanges().subscribe(
			(product) => {
				this.loading = false;
				// this.spinnerService.hide();
				this.shippingList = [];
				this.shippingCurrentList = [];
				product.forEach((element) => {
					const y = element.payload.toJSON();
					y['$key'] = element.key;
					this.shippingList.push(y as Shipping);
				});

				for(var i=0; i<this.shippingList.length; i++){
					if(this.shippingList[i].emailId == this.authService.getLoggedInUser().emailId){
						this.shippingCurrentList.push(this.shippingList[i]);
					}
				}
				this.numberOfShippings = this.shippingCurrentList.length;
				
				for(var i=0; i<this.shippingCurrentList.length;i++){
					this.currentProducts[i] = this.shippingCurrentList[i].products;
				}
				
				for(var r=0; r < this.currentProducts.length; r++)
				{
					this.currentZamowienie[r] = Object.entries(this.currentProducts[r]); 
				}
				//this.currentZamowienie[0] = Object.entries(this.currentProducts[3]); 

				//console.log("Aktualny", this.currentZamowienie[0][0][1]);
				//console.log("Testy", this.currentZamowienie);

				
			},
			(err) => {
				this.toastrService.error('Error while fetching Products', err);
			}
		);
	}

}
