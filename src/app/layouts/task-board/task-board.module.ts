import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TaskBoardComponent } from "./task-board.component";
import { SharedModule } from "src/app/shared/shared.module";
import { TaskRoutes } from "./task.routing";
import { AngularFireDatabase } from "angularfire2/database";

@NgModule({
  imports: [
    CommonModule, SharedModule, TaskRoutes],
  declarations: [TaskBoardComponent]
})
export class TaskBoardModule { 

  
}