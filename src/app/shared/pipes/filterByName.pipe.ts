import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "filterByName"
})
export class FilterByNamePipe implements PipeTransform {
  transform(items: any, select?: any): any {
    if (select !== "All") {
      return select
        ? items.filter(item => item["productName"].toLowerCase().includes(select.toLowerCase()))
        : items;
    } else {
      return items;
    }
  }
}
