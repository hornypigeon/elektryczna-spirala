export class Shipping {
    $key: string;
    address1: string;
    address2: string;
    emailId: string;
    firstName: string;
    lastName: string;
    products: [
      
    ]
    shippingDate: string; 
    state: string;
    totalPrice: number;
    userId: string;
    zip: string;
  }
  